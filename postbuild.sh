#!/bin/sh



## Set the tools path here
tool_path=~/outils/ESP/esptool
gcc_path=~/outils/esp-open-sdk/xtensa-lx106-elf/bin
#suffix=".bin"
suffix=""


# Flash size in megabits
# Valid values are same as for esptool.py - 2,4,8,16,32
#FLASH_SIZE ?= 16
FLASH_SIZE="4MB"

# Flash mode, valid values are same as for esptool.py - qio,qout,dio.dout
#FLASH_MODE ?= qio
FLASH_MODE="dio"

# Flash speed in MHz, valid values are same as for esptool.py - 80, 40, 26, 20
FLASH_SPEED="40"

# firmware tool arguments
ESPTOOL_ARGS="-fs ${FLASH_SIZE} -fm ${FLASH_MODE} -ff ${FLASH_SPEED}m"

# serial port settings for esptool.py
ESPPORT="/dev/ttyUSB0"
ESPBAUD="115200"

# rboot firmware binary paths for flashing
RBOOT_PREBUILT_BIN="${tool_path}/bootloader/firmware_prebuilt/rboot.bin"
RBOOT_CONF="${tool_path}/bootloader/firmware_prebuilt/blank_config.bin"
RBOOT_ARGS="0x0 ${RBOOT_PREBUILT_BIN} 0x1000 ${RBOOT_CONF}"

## Define project location and name
if [ $# == 1 ]; then
	project_root=$1
	debug_loc=${project_root}/Debug
	project_name=$(echo $1 | sed -e "s/^.*\///")
else
	if [ $(pwd | sed -e "s/^.*\///") == "Debug" ]; then
		debug_loc=$(pwd)
		cd ../
		project_root=$(pwd)
		cd ${debug_loc}
		project_name=$(echo ${project_root} | sed -e "s/^.*\///")
	else
		echo -e "\nERROR : no project path given !\n"
		exit -1
	fi
fi

## Create the .HEX file
#${gcc_path}/xtensa-lx106-elf-objcopy -O ihex ${debug_loc}/${project_name} ${debug_loc}/${project_name}.hex

## Create the assembly listing .LLS
#${gcc_path}/xtensa-lx106-elf-objdump -DS ${debug_loc}/${project_name} > ${debug_loc}/${project_name}.lss

## Create the files 
#python3 ${tool_path}/esptool.py elf2image $^
python3 ${tool_path}/esptool.py elf2image --version=2 ${ESPTOOL_ARGS} ${debug_loc}/${project_name}
#$(ESPTOOL) elf2image --version=2 $(ESPTOOL_ARGS) $< -o $(FW_FILE)

## Recovering the names given
#files=( $(dir *.bin) )
#addresses=()
#for i in ${files[*]}
#do
#addresses+=($(echo "$i" | sed -e "s/^.*-//g" -e "s/\..*$//g"))
#done
##echo ${addresses[0]} ${addresses[1]}

## programm the microcontroller
#python3 ${tool_path}/esptool.py write_flash ${addresses[0]} ${files[0]} ${addresses[1]} ${files[1]}
echo python3 ${tool_path}/esptool.py -p ${ESPPORT} --baud ${ESPBAUD} write_flash ${ESPTOOL_ARGS} 0x2000 ${debug_loc}/${project_name}-0x02000.bin
python3 ${tool_path}/esptool.py -p ${ESPPORT} --baud ${ESPBAUD} write_flash ${ESPTOOL_ARGS} 0x2000 ${debug_loc}/${project_name}-0x02000.bin
# 0 ${project_name}
#0x10000 ${project_name}
#$(ESPTOOL) -p $(ESPPORT) --baud $(ESPBAUD) write_flash $(ESPTOOL_ARGS) $(RBOOT_ARGS) 0x2000 $(FW_FILE) $(SPIFFS_ESPTOOL_ARGS)
