#!/bin/sh

#For local debuging, start a server :
#$ ncat -l 10001 --keep-open --udp --exec "/bin/true" --output "/dev/stdout"

precision=0
step=1
DC=0
auto=0
send=0
err=" "

IP=""
PORT=""
IP="192.168.1.161"
#IP="localhost"
PORT="10001"

message=""

if [ -z ${IP} ]; then
    echo "IP address :"
    read -ip IP
fi
if [ -z ${PORT} ]; then
    echo "Port :"
    read -ip PORT
fi

while  [ "${C}" != "q" ]; do
    clear
    echo $err
    err=" "
    echo "Messages destination : ${IP}:${PORT}"
    echo "[Q] to quit. Step:${step} [Z]=step*10 [S]=step/10"
    echo "[I]=DC++"
    echo "[K]=DC--"
    echo "auto Send:${auto} [A]=toggle. [E]=send."
    echo -e "DC:${DC}"
    
    read -s -n 1 C
    case $C in
    'i')
        DC=$(echo "scale=${precision};${DC}+${step}" | bc)
        if [ ${DC} -gt 100 ]; then
            DC=100
        fi
        if [ ${DC} -gt 52 ]; then
            DC=52
        fi
        err=" "
        ;;
    'k')
        DC=$(echo "scale=${precision};${DC}-${step}" | bc)
        if [ ${DC} -lt 2 ]; then
            DC=2
        fi
        err=" "
        ;;
    'p')
        if [ ${DC} -gt 2 ]; then
            DC=2
        else
            DC=50
        fi
        err=" "
        ;;
    'a')
        if [ 0 -eq ${auto} ]; then
            auto=1
        else
            auto=0
        fi
        err=" "
        ;;
    'e')
        if [ 0 -eq ${auto} ]; then
            send=1
            err=" "
        else
            err="Auto mode !"
        fi
        ;;
    'z')
        step=$(echo "scale=${precision};${step}*10" | bc)
        if [ ${step} -gt 10 ]; then
            step=10
        fi
        err=" "
        ;;
    's')
        step=$(echo "scale=${precision};${step}/10" | bc)
        if [ ${step} -lt 1 ]; then
            step=1
        fi
        err=" "
        ;;
    *)
        err="Character not allowed."
        ;;
    esac

    if [ 1 -eq ${auto} ] || [ 1 -eq ${send} ]; then
        send=0
        message=$(echo -e "{ \"DC\": ${DC} } ")
        echo ${message} | ncat -C -u ${IP} ${PORT}
        err=$(echo ${message})
    fi

done
