#!/bin/sh

#For local debuging, start a server :
#$ ncat -l 10001 --keep-open --udp --exec "/bin/true" --output "/dev/stdout"

precision=3
step=0.01
kP=0
kI=0
kD=0
auto=0
send=0
err=" "

IP=""
PORT=""
IP="192.168.1.161"
#IP="localhost"
PORT="10001"

message=""

if [ -z ${IP} ]; then
    echo "IP address :"
    read -ip IP
fi
if [ -z ${PORT} ]; then
    echo "Port :"
    read -ip PORT
fi

while  [ "${C}" != "q" ]; do
    clear
    echo $err
    err=" "
    echo "Messages destination : ${IP}:${PORT}"
    echo "[Q] to quit. Step:${step} [Z]=step*10 [S]=step/10"
    echo "[I]=kP++ [O]=kI++ [P]=kD++"
    echo "[K]=kP-- [L]=kI-- [M]=kD--"
    echo "auto Send:${auto} [A]=toggle. [E]=send."
    echo -e "kP:${kP}\tkI:${kI}\tkD=${kD}"
    
    read -s -n 1 C
    case $C in
    'i')
        kP=$(echo "scale=${precision};${kP}+${step}" | bc)
        err=" "
        ;;
    'k')
        kP=$(echo "scale=${precision};${kP}-${step}" | bc)
        err=" "
        ;;
    'o')
        kI=$(echo "scale=${precision};${kI}+${step}" | bc)
        err=" "
        ;;
    'l')
        kI=$(echo "scale=${precision};${kI}-${step}" | bc)
        err=" "
        ;;
    'p')
        kD=$(echo "scale=${precision};${kD}+${step}" | bc)
        err=" "
        ;;
    'm')
        kD=$(echo "scale=${precision};${kD}-${step}" | bc)
        err=" "
        ;;
    'a')
        if [ 0 -eq ${auto} ]; then
            auto=1
        else
            auto=0
        fi
        err=" "
        ;;
    'e')
        if [ 0 -eq ${auto} ]; then
            send=1
            err=" "
        else
            err="Auto mode !"
        fi
        ;;
    'z')
        step=$(echo "scale=${precision};${step}*10" | bc)
        err=" "
        ;;
    's')
        step=$(echo "scale=${precision};${step}/10" | bc)
        if [ ${step} -eq 0 ]; then
            step=1
        fi
        err=" "
        ;;
    *)
        err="Character not allowed."
        ;;
    esac

    if [ 1 -eq ${auto} ] || [ 1 -eq ${send} ]; then
        send=0
        message=$(echo -e "{ \"regulator\": { \"kP\": ${kP}, \"kI\": ${kI}, \"kD\": ${kD} } } ")
        echo ${message} | ncat -C -u ${IP} ${PORT}
        err=$(echo ${message})
    fi

done
