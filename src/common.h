/*
 * common.h
 *
 *  Created on: 2 janv. 2019
 *      Author: Jean-Baptiste Biernacki
 */

#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#include <c_types.h> //uintX_t NULL
#include <FreeRTOS.h> //TickType_t and queue.h
#include <queue.h>//For QueueHandle_t

#include "ipc.h" //messageHandler_t
//#include "regulator.h" //COEF_t

//Defining the type of the coefficients once.
typedef float COEF_t;

typedef enum {
	PID_KP,
	PID_KI,
	PID_KD,
	PID_BT,
	PID_CONSIGNE
} pidenum_t;

typedef struct{
	pidenum_t parameter;
	COEF_t value;
	uint16_t consigne;
	TickType_t time;
} pidparam_t;


extern QueueHandle_t RegulatorQueueHDL;
extern uint16_t newConsigne;
extern messageHandler_t listenerMessage;
extern uint16_t shared;

#endif /* SRC_COMMON_H_ */
