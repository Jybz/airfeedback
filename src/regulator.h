/*
 * regulation.h
 *
 *  Created on: 28 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */

#ifndef SRC_REGULATOR_H_
#define SRC_REGULATOR_H_

#include <FreeRTOS.h>
#include <task.h>
#include <stdio.h>
#include <espressif/esp_system.h>

#include "pwm/pwm.h"
#include "ipc.h"
#include "common.h"


#define REGULATOR_STACK_SIZE 512

#define PWM_NB_CHANNEL 1
#define SERVO_PORT 4
#define SERVO_FREQ 400


typedef struct regulator{
	COEF_t kP;
	COEF_t kI;
	COEF_t kD;
	uint16_t consigne;
	TickType_t timeBase;

	COEF_t delta;
	COEF_t sigma;
} regulator_t;


extern TaskHandle_t regulatorTaskHandler;
void regulatorTask(void *pvParameters);


#endif /* SRC_REGULATOR_H_ */
