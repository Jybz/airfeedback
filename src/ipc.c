/*
 * ipc.c
 *
 *  Created on: 29 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */

#include "ipc.h"

uint8_t initMessage(messageHandler_t* messageHandler, TaskHandle_t pTaskHandler){
	uint8_t retval=0;
	if(pTaskHandler&&messageHandler){ //Check if pointers are not null.
		messageHandler->taskOwnership = pTaskHandler;
		messageHandler->size = 0;
		messageHandler->read = 1;
		messageHandler->pMessage = NULL;
		retval=1;
	}
	return retval;
}


uint8_t pendMessage(messageHandler_t* pMessageHDL){
	uint8_t retval=0;
	vTaskSuspend(pMessageHDL->taskOwnership);
	//vTaskSuspend(NULL);
	retval=pMessageHDL->read;
	return retval;
}


uint8_t sendMessage(messageHandler_t* pMessageHDL, void* message, uint32_t messageSize){
	uint8_t retval=0;
	if(pMessageHDL){ //Ensure pMessageHDL is defined.
		if(pMessageHDL->read){ //If the message is not overwritten
			pMessageHDL->pMessage = message;
			pMessageHDL->size = messageSize;
			pMessageHDL->read = 0;
			vTaskResume( (pMessageHDL->taskOwnership ) );
			retval = 1;
		}
	}
	return retval;
}

void* getMessage(messageHandler_t* pMessageHDL){
	pMessageHDL->read = 1;
	return (pMessageHDL->pMessage);
}
