/*
 * regulation.c
 *
 *  Created on: 28 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */
#include "regulator.h"

//#define DEBUG "\tDEBUG PID "
#ifdef DEBUG
#define debug(fmt, ...) printf("%s: " fmt "\n", DEBUG, ## __VA_ARGS__)
#else
#define debug(fmt, ...)
#endif

#define SIZE_QUEUE 5

TaskHandle_t regulatorTaskHandler;

void regulatorTask(void *pvParameters)
{
	//Init
	uint8_t servo=SERVO_PORT;
	uint16_t adc_read, p_adc_read;
	uint32_t output=0, p_output=1;

	regulator_t iRegulator;
	//int16_t epsilon, pEpsilon;
	COEF_t epsilon, pEpsilon;
	TickType_t xLastWakeTime;
	pidparam_t newParameter;
	iRegulator.timeBase = 10 / portTICK_PERIOD_MS;
	iRegulator.kP = 1;
	iRegulator.kI = 0;
	iRegulator.kD = 0;
	//	iRegulator.consigne = 65535>>1;
	iRegulator.consigne = 0;
	//iRegulator.consigne = 26214;
	COEF_t tmpP;
	COEF_t tmpI;
	COEF_t tmpD;

	pwm_init(PWM_NB_CHANNEL, &servo, false);
	//pwm_init(PWM_NB_CHANNEL, &servo, true);
	pwm_set_freq(SERVO_FREQ);
	pwm_set_duty(0);
	pwm_start();

	RegulatorQueueHDL = xQueueCreate( SIZE_QUEUE, sizeof( pidparam_t ) );

	//Initialise the value
	xLastWakeTime = xTaskGetTickCount();
	//task
    while(1) {

    	// ##############################
    	// # RECOVERING NEW PARAMETERS  #
    	// ##############################
    	//get messages for kP KI kD & Time-Base
    	//And the order
    	while( pdTRUE == xQueueReceive(RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) ){
    		switch(newParameter.parameter){
    		case PID_KP:
    			debug("Old kP : %f", iRegulator.kP);
    			iRegulator.kP = newParameter.value;
    			debug("New kP : %f", iRegulator.kP);
    			break;
    		case PID_KI:
    			debug("Old kI : %f", iRegulator.kI);
    			iRegulator.kI = newParameter.value;
    			debug("New kI : %f", iRegulator.kI);
    			break;
    		case PID_KD:
    			debug("Old kD : %f", iRegulator.kD);
    			iRegulator.kD = newParameter.value;
    			debug("New kD : %f", iRegulator.kD);
    			break;
    		case PID_BT:
    			debug("Old TB : %d", iRegulator.timeBase);
    			iRegulator.timeBase = (newParameter.time/portTICK_PERIOD_MS);
    			debug("New kP : %d", iRegulator.timeBase);
    			break;
    		case PID_CONSIGNE:
    			debug("Old Order : %d", iRegulator.consigne);
    			iRegulator.consigne = newParameter.consigne;
    			debug("New Order : %d", iRegulator.consigne);
    			break;
    		default:
    			//WOOT ? Not possible.
    			debug("What ?!");
    			break;
    		}
    	}


    	// ##############################
    	// #    MEASURING THE VALUE     #
    	// ##############################
		//The ADC give a value back from 0 to 1024 (which is not possible,
		//normally, only up to 1023.
    	adc_read = sdk_system_adc_read();
    	//debug("raw adc : %d",adc_read);
    	if(adc_read)adc_read--;   //If the value is not 0, then not -1
    	adc_read=adc_read<<6; 	  //We multiply by 64, for a wider range
    							  //Instead 0-1023(step 1) => 0-65472(step 64)
//    	if(p_adc_read != adc_read){
//    		p_adc_read = adc_read;
//    		output = adc_read;
//    	}


    	// ##############################
    	// # COMPUTING NEW OUTPUT VALUE #
    	// ##############################
    	//Error = TargetValue - Measurement
    	//epsilon = iRegulator.consigne - adc_read;
    	epsilon = iRegulator.consigne - 0;
    	tmpP = epsilon * iRegulator.kP;
//    	// I Part = sum of error
//    	if(0 < epsilon){
//    		if(iRegulator.sigma > iRegulator.sigma+epsilon){
//    			iRegulator.sigma = FLOAT_MAX;
//    		}else{
//    			iRegulator.sigma += epsilon;
//    		}
//    	}else if(0 > epsilon){
//    		if(iRegulator.sigma < iRegulator.sigma+epsilon){
//    			iRegulator.sigma = FLOAT_MAX;
//    		}else{
//    			iRegulator.sigma += epsilon;
//    		}
//    	}else{
//    		iRegulator.sigma +=epsilon;
//    	};
		iRegulator.sigma +=epsilon;
    	//if(iRegulator.sigma < iRegulator.sigma+epsilon)
    	//iRegulator.sigma +=epsilon;
		tmpI = iRegulator.sigma*iRegulator.kI;
    	// D part = error - last error
    	iRegulator.delta = epsilon-pEpsilon;
    	tmpD = iRegulator.delta*iRegulator.kD;
    	pEpsilon = epsilon;
    	//Compute output
    	output = (tmpP) + (tmpI) + (tmpD);
    	//debug("DCcycle:%d",output);
    	if(output>65535/2){
    		output=65535/2;
    	}
    	if(output<2){
    		output=2;
    	}

    	//debug("");
//    	debug("\033[2J Adc feedback : %d",adc_read);
//    	debug("Old output : %d",output);
//    	debug("New order : %d",iRegulator.consigne);
//    	debug("Error : %d", epsilon);
//    	debug("sigma : %f", iRegulator.sigma);
//    	debug("delta : %f", iRegulator.delta);
//    	debug("new output : %d",output);

#define PIDdebug 0
#if PIDdebug
    	if(tmp>10){
    		tmp=0;
        	//debug("\033[2J\n)");
        	debug("Cs:%d\tadc:%d\tP:%f\tI:%d\tD:%f\tOut:%d",iRegulator.consigne, adc_read, tmpP, tmpI, tmpD, output);
    	}
    	tmp++;
#endif
    	// ##############################
    	// #      REFRESH THE PWM       #
    	// ##############################
    	//As changing the duty reset the softPWM, I prefer changing it only if it really need to be reseted.
    	if(output != p_output){
			debug("New Duty Cycle : %d", output);
    		pwm_set_duty(output);
    		p_output = output;
    	}


    	// ##############################
    	// #      WAIT PERIODICALY      #
    	// ##############################
    	//Wake up regulary, "iRegulator.timeBase" time after the previous wakeup.
    	//The wokeup time is updated inside the variable xLastWakeTime
    	vTaskDelayUntil(&xLastWakeTime, iRegulator.timeBase);
    	//vTaskDelay(iRegulator.timeBase);
    }
}
