/*
 * listener.h
 *
 *  Created on: 28 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */

/*
 * There is two needed messages :
 * ·One for the parameters of the PID
 * ·One for the new position or in other words the new desired value.
 *
 * The expected string for the parameters is :
 * { "regulator" :	{
 * 					"kP" : 0.89,
 * 					"kI" : 0.05,
 * 					"kD" : 200.4,
 * 					"sampleTime" : 100
 * 					}
 * }
 *
 * The expected string for the position is :
 * { "position" :	{
 * 					"absoluteX" : 15.6,
 * 					"absoluteY" : 0.25,
 * 					"absoluteZ" : 1.05
 * 					}
 * }
 *
 *
 * The expected string for a new duty cycle :
 * { "DC" : 100
 * }
 *
 *
 * Nothing else planed yet.
 *
 * Zut... I just thought, we need a new structure and many other things to set the minimum/maximum points.
 * The expected string for the points is :
 * { "maxPoint" :	{
 * 					"maxX" : 10.0,
 * 					"maxY" : 10.0,
 * 					"maxZ" : 1.50
 * 					},
 * 	"minPoint" :	{
 * 					"minX" : 0.0,
 * 					"minY" : 0.0,
 * 					"minZ" : 1.50
 * 					}
 * }
 *
 */

#ifndef SRC_LISTENER_H_
#define SRC_LISTENER_H_

#include <c_types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include "jsmn/jsmn.h"
#include "ipc.h"
#include "common.h"

#define LISTENER_STACK_SIZE 512

#define TOKEN_MAX 20

#define STR_REGULATOR_OBJ "regulator"
#define STR_REGULATOR_KP  "kP"
#define STR_REGULATOR_KI  "kI"
#define STR_REGULATOR_KD  "kD"
#define STR_REGULATOR_ST  "sampleTime"

#define STR_POSITION_OBJ "position"
#define STR_POSITION_PX  "absoluteX"
#define STR_POSITION_PY  "absoluteY"
#define STR_POSITION_PZ  "absoluteZ"

#define STR_SETTING_OBJ_MAX "maxPoint"
#define STR_SETTING_MAX_X   "maxX"
#define STR_SETTING_MAX_Y   "maxY"
#define STR_SETTING_MAX_Z   "maxZ"
#define STR_SETTING_OBJ_MIN "minPoint"
#define STR_SETTING_MIN_X   "minX"
#define STR_SETTING_MIN_Y   "minY"
#define STR_SETTING_MIN_Z   "minZ"

#define regulParamTest 0
#if regulParamTest
extern const char* ParamJsonTest;
#endif
#define regulPositTest 0
#if regulPositTest
extern const char* PositJsonTest;
#endif
#define regulSettiTest 0
#if regulSettiTest
extern const char* PositSettiTest;
#endif

extern TaskHandle_t listenerTaskHandler;
void listenerTask(void *pvParameters);

#endif /* SRC_LISTENER_H_ */
