/* The classic "blink" example
 *
 * This sample code is in the public domain.
 */

#include <stdlib.h>
#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <FreeRTOS.h>
#include <task.h>
//#include <esp8266.h>
#include <string.h>
#include <stdio.h>
#include <espressif/esp_wifi.h>
#include <espressif/esp_sta.h>

#include <lwip/udp.h>
#include <lwip/sockets.h>

//#include <queue.h>

#include "common.h"
#include "listener.h"
#include "regulator.h"
#include "ipc.h"

//Tricky thing found in the PWM.c lib :
#define DEBUG "\tWiFi "
#ifdef DEBUG
#define debug(fmt, ...) printf("%s: " fmt "\n", DEBUG, ## __VA_ARGS__)
#else
#define debug(fmt, ...)
#endif

#define WIFI_SSID "esewifi"
#define WIFI_PASS "silkykayak943"
#define WIFI_PORT_LISTENING 10001
#define WIFI_STACK_SIZE 512
TaskHandle_t wifiTaskHandler;

#define BUFF_SIZE 50
void manualTask(void *pvParameters)
{
	//Init
	//uint32_t inBuff=0;
	uint8_t inBuffs[BUFF_SIZE]="Ceci est un test !";
	//uint16_t intRead=0;

	//RegulatorQueue = xQueueCreate( 10, sizeof( uint16_t ) );
	//RegulatorQueue = xQueueGenericCreate(,,);

	//task
//    while(0) {
//    	scanf("%d", &inBuff);
//    	intRead=(uint16_t)(inBuff&0x0FFFF);
//    	//xQueueSend(RegulatorQueue , (void*) &intRead, 0);
//    	shared = inBuff;
//    	vTaskDelay(1000 / portTICK_PERIOD_MS);
//    }

//    while(0){
//    	vTaskDelay(5000 / portTICK_PERIOD_MS);
//    	sendMessage(&listenerMessage, inBuffs, BUFF_SIZE);
//    }

    while(1){
    	scanf("%s", inBuffs);
    	if(0)
    		debug("%s", inBuffs);
    	//vTaskDelay(5000 / portTICK_PERIOD_MS);
#if regulParamTest
    	sendMessage(&listenerMessage, (void*)ParamJsonTest, strlen(ParamJsonTest));
#endif


#if regulPositTest
    	sendMessage(&listenerMessage, (void*)PositJsonTest, strlen(PositJsonTest));
#endif


#if regulSettiTest
    	sendMessage(&listenerMessage, (void*)PositSettiTest, strlen(PositSettiTest));
#endif

    }
}

//code from the example "esp-open-rtos/examples/upnp/upnp_test.c"
/**
  * @brief This function is called when an UDP datagrm has been received on the port UDP_PORT.
  * @param arg user supplied argument (udp_pcb.recv_arg)
  * @param pcb the udp_pcb which received data
  * @param p the packet buffer that was received
  * @param addr the remote IP address from which the packet was received
  * @param port the remote port from which the packet was received
  * @retval None
  */
static void receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
{
    if (p) {
    	debug("Msg received port:%d len:%d\n", port, p->len);
        uint8_t *buf = (uint8_t*) p->payload;
        debug("Msg received port:%d len:%d\nbuf: %s\n", port, p->len, buf);

        //send_udp(upcb, addr, port);
        sendMessage(&listenerMessage, p->payload, p->len);

        pbuf_free(p);
    }
}

//Code from the same example, but other file : "esp-open-rtos/examples/upnp/upnp.c"
//Oh wait... I totally rewrote it...
/**
  * @brief This is the wifi connection task
  * @param arg user supplied argument from xTaskCreate
  * @retval None
  */
static void wifiTask(void *pvParameters)
{
    struct udp_pcb *upcb;
    struct sdk_station_config config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASS,
    };

    debug("Connection to WiFi ordered.");
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&config);


    upcb = udp_new();
	if (!upcb) {
		debug("Error, udp_new failed");
	}else{
		//might be moved in the STATION_GOT_IP case.
		udp_bind(upcb, IP4_ADDR_ANY, (uint16_t) WIFI_PORT_LISTENING);
        udp_recv(upcb, receive_callback, NULL);
    }

    while(1){
    	switch( sdk_wifi_station_get_connect_status() ){
    	case STATION_IDLE:
    		debug("Do nothing.");
    		break;
        case STATION_CONNECTING:
        	debug("Connecting...");
    		break;
        case STATION_WRONG_PASSWORD:
        	debug("wrong password.");
    		break;
        case STATION_NO_AP_FOUND:
        	debug("Access Point not found.");
    		break;
        case STATION_CONNECT_FAIL:
        	debug("Connection failed.");
    		break;
        case STATION_GOT_IP:
        	debug("Connected ! \\o/");
        	while(STATION_GOT_IP == sdk_wifi_station_get_connect_status()){
        		//Change the context.
        		taskYIELD();
        	}
        	debug("WiFi disconnected /o\\ ...");
        	sdk_wifi_station_disconnect();
        	//Not sure if necessary :
        	sdk_wifi_set_opmode(STATION_MODE);
        	sdk_wifi_station_set_config(&config);
    		break;
        default:
        	//lol not possible.
        	break;
    	}
    	vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    udp_remove(upcb);
    upcb = NULL;
}

void user_init(void)
{
    uart_set_baud(0, 115200);

    //xTaskCreate(manualTask,    "manual Task",    WIFI_STACK_SIZE, NULL,      2, &wifiTaskHandler      );
    xTaskCreate(wifiTask,      "Wifi Task",      WIFI_STACK_SIZE, NULL,      2, &wifiTaskHandler      );
    xTaskCreate(listenerTask,  "Listener Task",  LISTENER_STACK_SIZE, NULL,  2, &listenerTaskHandler  );
    xTaskCreate(regulatorTask, "Regulator Task", REGULATOR_STACK_SIZE, NULL, 2, &regulatorTaskHandler );
}
