/*
 * ipc.h
 *
 *  Created on: 28 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */

#ifndef SRC_IPC_H_
#define SRC_IPC_H_

#include <c_types.h> //uintX_t NULL
#include <FreeRTOS.h> //Needed for task.h
#include <task.h>	//suspend, resume, ...
//#include <queue.h>
//#include "common.h"
//#include <espressif/queue.h>
//#include <stdio.h>

typedef struct messageHandler{
	TaskHandle_t taskOwnership;	//For which task is defined this message queue.
	void* pMessage;				//A type-less buffer.
	uint32_t size;				//The size of the transmitted message.
	uint8_t read;				//control value to prevent overwriting and lost.
} messageHandler_t;

uint8_t initMessage(messageHandler_t* messageHandler,TaskHandle_t pTaskHandler);

uint8_t pendMessage(messageHandler_t* pMessageHDL);

uint8_t sendMessage(messageHandler_t* pMessageHDL, void* message, uint32_t messageSize);

void* getMessage(messageHandler_t* pMessageHDL);

#endif /* SRC_IPC_H_ */
