/*
 * listener.c
 *
 *  Created on: 28 déc. 2018
 *      Author: Jean-Baptiste Biernacki
 */

#include "listener.h"


#define DECIMAL_BASE 10

//Tricky thing found in the PWM.c lib :
//#define DEBUG "\tDEBUG JSON "
#ifdef DEBUG
#define debug(fmt, ...) printf("%s: " fmt "\n", DEBUG, ## __VA_ARGS__)
#else
#define debug(fmt, ...)
#endif


#if regulParamTest
const char* ParamJsonTest = "\n{\t\""STR_REGULATOR_OBJ"\":\t{\n"
									"\t\t\""STR_REGULATOR_KP"\": 0.89,\n"
									"\t\t\""STR_REGULATOR_KI"\": 0.05,\n"
									"\t\t\""STR_REGULATOR_KD"\": 200.4,\n"
									"\t\t\""STR_REGULATOR_ST"\": 100\n"
								"\t}\n"
							"}\n";
#endif

#if regulPositTest
const char* PositJsonTest = "\n{\t\""STR_POSITION_OBJ"\":\t{\n"
									"\t\t\""STR_POSITION_PX"\": 15.6,\n"
									"\t\t\""STR_POSITION_PY"\": 0.25,\n"
									"\t\t\""STR_POSITION_PZ"\": 1.05\n"
								"\t}\n"
							"}\n";
#endif

#if regulSettiTest
const char* PositSettiTest = "\n{\t\""STR_SETTING_OBJ_MAX"\":\t{\n"
									"\t\t\""STR_SETTING_MAX_X"\": 20.0,\n"
									"\t\t\""STR_SETTING_MAX_Y"\": 20.0,\n"
									"\t\t\""STR_SETTING_MAX_Z"\": 1.50\n"
								"\t},\n"
								"\t\""STR_SETTING_OBJ_MIN"\":\t{\n"
									"\t\t\""STR_SETTING_MIN_X"\": 0.0,\n"
									"\t\t\""STR_SETTING_MIN_Y"\": 0.0,\n"
									"\t\t\""STR_SETTING_MIN_Z"\": 1.50\n"
								"\t}\n"
							"}\n";
#endif

typedef struct{
	float pointX;
	float pointY;
	float pointZ;
} point_t;

typedef struct{
	char pointX;
	char pointY;
	char pointZ;
} point_enable_t;

TaskHandle_t listenerTaskHandler;


//Function from the esp-open-rtos sdk for comparing strings. Result is modified, 1 (true) if equal, 0 (false) if not equal.
static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
	if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
			strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
		return 1;
	}
	return 0;
}

//Before regulation, what is the limit of the pressure ?
//65536 32768
#define POSITION_LIMIT_ORDER 32768

//TODO Find or fill the value for 1 centimeter.
#define POSITION_CM 1

//Give a minimum for delta of position
#define POSITION_LIMIT_DELTA 10/POSITION_CM

#define POSITION_USE_X 1
#define POSITION_USE_Y 1
#define POSITION_USE_Z 1

//From my experiences, this function take less then 1 systick (>1ms), sometime 10ms, I guess the function is cut to change the environment/task, or any other interrupt.
uint16_t computeConsigne(point_t* current, point_t* pointCoef, point_enable_t* enable){
	uint16_t retval = 0;
	float tmpX, tmpY, tmpZ, tmpRet=0;
	//printf("\tEntering fct:%d\n",(int)xTaskGetTickCount());

	tmpX = current->pointX*pointCoef->pointX;
	tmpY = current->pointY*pointCoef->pointY;
	tmpZ = current->pointZ*pointCoef->pointZ;

	if(enable->pointX)
		tmpRet = sqrtf((tmpX*tmpX)+(tmpRet*tmpRet));
	if(enable->pointY)
		tmpRet = sqrtf((tmpY*tmpY)+(tmpRet*tmpRet));
	if(enable->pointZ)
		tmpRet = sqrtf((tmpZ*tmpZ)+(tmpRet*tmpRet));

	retval = (uint16_t) tmpRet;
	//printf("\tComputed fct:%d\n",(int)xTaskGetTickCount());
	//printf("\tRetour:%d\n",retval);
	return(retval);
}

void computeCoef(point_t* pointCoef, point_t* max, point_t* min, point_enable_t* enable){
	//printf("\tEntering fct:%d\n",(int)xTaskGetTickCount());

	if(POSITION_LIMIT_DELTA<(max->pointX-min->pointX)){
		enable->pointX=TRUE;
		pointCoef->pointX = (POSITION_LIMIT_ORDER/(max->pointX-min->pointX));
	} else enable->pointX=FALSE;

	if(POSITION_LIMIT_DELTA<(max->pointY-min->pointY)){
		enable->pointY=TRUE;
		pointCoef->pointY = (POSITION_LIMIT_ORDER/(max->pointY-min->pointY));
	} else enable->pointY=FALSE;

	if(POSITION_LIMIT_DELTA<(max->pointZ-min->pointZ)){
		enable->pointZ=TRUE;
		pointCoef->pointZ = (POSITION_LIMIT_ORDER/(max->pointZ-min->pointZ));
	} else enable->pointZ=FALSE;

	debug("Coef_X : %f\tCoef_Y : %f\tCoef_Z : %f",pointCoef->pointX,pointCoef->pointY,pointCoef->pointZ);

	//printf("\tComputed fct:%d\n",(int)xTaskGetTickCount());
}


void listenerTask(void *pvParameters)
{
	char update=0, resetCoef=0;
	char tmpBuffer[10];
	point_enable_t enable;
	uint16_t itemNo, subItemNo;
	sint16_t itemTot;
	pidparam_t newParameter;
	jsmn_parser parser;
	//jsmntok_t tokList[TOKEN_MAX];
	jsmntok_t* freeTok;
	float tmpCoef;
	TickType_t tmpTime;

	uint8_t getNewOrder = 0;
	uint16_t newOrder = 0;

	point_t pointMax;
	point_t pointMin;
	point_t current;
	point_t pointCoef;

	//Init
	initMessage(&listenerMessage, listenerTaskHandler);
	jsmn_init(&parser);
	pointMin.pointX=0;
	pointMin.pointY=0;
	pointMin.pointZ=0;
	pointMax.pointX=10;
	pointMax.pointY=10;
	pointMax.pointZ=10;
	current.pointX=0;
	current.pointY=0;
	current.pointZ=0;
	enable.pointX=TRUE;
	enable.pointY=TRUE;
	enable.pointZ=TRUE;

	//Task
    while(1) {
    	jsmntok_t tokList[TOKEN_MAX];
    	jsmn_init(&parser);
    	pendMessage(&listenerMessage);
    	debug("The message is : %s", (uint8_t*)getMessage(&listenerMessage));
    	debug("msg size :%d",listenerMessage.size);

        itemTot = jsmn_parse(&parser, getMessage(&listenerMessage), listenerMessage.size, tokList, TOKEN_MAX);
        if(itemTot<0){
        	debug("Failed to parse:%d\n",itemTot);
        }
        else if (itemTot < 1 || tokList[0].type != JSMN_OBJECT) {
        	debug("Object expected\n");
    	}
        else {
        	itemNo = 1;
        	//for (itemNo = 1; itemNo < itemTot; itemNo++) {
        		debug("Object:");
        		debug("%.*s", tokList[1].end-tokList[1].start, (char*)listenerMessage.pMessage + tokList[1].start);

				// ##############################
				// #  NEW POSITION COORDINATE   #
				// ##############################
        		if(jsoneq(listenerMessage.pMessage, &tokList[itemNo], STR_POSITION_OBJ)) {
        			//printf("\tOBJ POSITION GOT.\n");
        			update=1;
        			for(subItemNo=2; subItemNo<(((tokList[itemNo+1].size)<<1)+2); subItemNo=subItemNo+2){
        				//At this point, we have only the content of the object STR_REGULATION_OBJ with the brackets { ... } in "tokList[itemNo+1]"
        				freeTok = &tokList[itemNo+subItemNo];
        				//printf("\tFOR LOOP.\n");
            			if(jsoneq(listenerMessage.pMessage, freeTok, STR_POSITION_PX)){
        					//printf("\tpX detected !\n");
            				if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
            					//printf("\t\tcontent: %s\n", tmpBuffer);
            					current.pointX = (float)strtod(tmpBuffer, NULL);
            					//printf("\t\tback: %f\n", tmpPx);
            				}
            			}else
            			if(jsoneq(listenerMessage.pMessage, freeTok, STR_POSITION_PY)){
        					//printf("\tpX detected !\n");
            				if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
            					//printf("\t\tcontent: %s\n", tmpBuffer);
            					current.pointY = (float)strtod(tmpBuffer, NULL);
            					//printf("\t\tback: %f\n", tmpPy);
            				}
            			}else
            			if(jsoneq(listenerMessage.pMessage, freeTok, STR_POSITION_PZ)){
        					//printf("\tpX detected !\n");
            				if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
            					//printf("\t\tcontent: %s\n", tmpBuffer);
            					current.pointZ = (float)strtod(tmpBuffer, NULL);
            					//printf("\t\tback: %f\n", tmpPz);
            				}
            			}//else printf("Error in the position subItems!\n");
        			}
        		}else

				// ##############################
				// #      NEW DUTY CYCLE        #
				// ##############################
				if(jsoneq(listenerMessage.pMessage, &tokList[itemNo], "DC")) {
					//printf("\tOBJ DC GOT.\n");
					debug("NEW ORDER OBJECT");
					freeTok = &tokList[itemNo];
					if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
						newOrder = (uint16_t) ((float)strtod(tmpBuffer, NULL) * 655.35);
						getNewOrder=1;
						debug("NEW ORDER : %d",newOrder);
					}


				}else


        		// ##############################
        		// # NEW REGULATION PARAMETERS  #
        		// ##############################
        		if (jsoneq(listenerMessage.pMessage, &tokList[itemNo], STR_REGULATOR_OBJ)) {
        			debug("Objet Regulation Detecte.\n");
        			//The message obtained is new values for the PID parameters.
        			//itemNo is "regulator", ItemNo+1 is its content.
        			//if subItemNo=0, it makes reference to "regulator", with 1 if refers to the unique content {}, with 2 to the first content.
        			//As the item counts for keys and for associated value, we have to double the value. Of course, as we start with +2, with have to stop with +2.
        			//printf("Size of tokList[itemNo+1].size : %d\n", tokList[itemNo+1].size);
        			for(subItemNo=2; subItemNo<(((tokList[itemNo+1].size)<<1)+2); subItemNo=subItemNo+2){
        				//At this point, we have only the content of the object STR_REGULATION_OBJ with the brackets { ... } in "tokList[itemNo+1]"
        				freeTok = &tokList[itemNo+subItemNo];
        				//printf("Content of freeTok : %.*s\n", freeTok[subItemNo].end-freeTok[subItemNo].start, (char*)listenerMessage.pMessage + freeTok[subItemNo].start);
        				//printf("Content of freeTok : %.*s\n", freeTok[0].end-freeTok[0].start, (char*)listenerMessage.pMessage + freeTok[0].start);

        				if(jsoneq(listenerMessage.pMessage, freeTok, STR_REGULATOR_KP)){
        					//printf("\tKp detecte !\n");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								//printf("\tin the kP procesing : %s\n",tmpBuffer);
								//Version 1 atoff
								//tmpCoef = atof(tmpBuffer);
								//Version 2 strtof with a second parameter, a pointer to the next character after the number, not needed, we can use NULL.
								//tmpCoef = strtof(tmpBuffer, NULL);
								tmpCoef = (float)strtod(tmpBuffer, NULL);
								newParameter.parameter = PID_KP;
								newParameter.value = tmpCoef;
								//printf("\tvalue back: %f\n",tmpCoef);
								//From the API description, xQueueSend : The item is queued by copy, not by reference.
								if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
								{
									// Failed to post the message.
								}//else printf("Send with success!\n");
							}
        				}else
        				if(jsoneq(listenerMessage.pMessage, freeTok, STR_REGULATOR_KI)){
        					//printf("\tKI detecte !\n");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								//printf("\tin the kI procesing : %s\n",tmpBuffer);
								tmpCoef = (float)strtod(tmpBuffer, NULL);
								newParameter.parameter = PID_KI;
								newParameter.value = tmpCoef;
								//printf("\tvalue back: %f\n",tmpCoef);
								if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
								{
									// Failed to post the message.
								}//else printf("Send with success!\n");
							}
        				}else
        				if(jsoneq(listenerMessage.pMessage, freeTok, STR_REGULATOR_KD)){
        					//printf("\tKD detecte !\n");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								//printf("in the kD procesing : %s\n",tmpBuffer);
								tmpCoef = (float)strtod(tmpBuffer, NULL);
								newParameter.parameter = PID_KD;
								newParameter.value = tmpCoef;
								//printf("\tvalue back: %f\n",tmpCoef);
								if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
								{
									// Failed to post the message.
								}//else printf("Send with success!\n");
							}
        				}else
        				if(jsoneq(listenerMessage.pMessage, freeTok, STR_REGULATOR_ST)){
        					//printf("\tBaseTime detecte !\n");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								//printf("in the BT procesing : %s\n",tmpBuffer);
								tmpTime = (TickType_t) strtol(tmpBuffer, NULL, DECIMAL_BASE);
								newParameter.parameter = PID_BT;
								newParameter.time = tmpTime;
								//printf("\tvalue back: %d\n",tmpTime);
								if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
								{
									// Failed to post the message.
								}//else printf("Send with success!\n");
							}
        				}//else printf("Error in the regulation subItems!\n");
        			}
        		}else


				// ##############################
				// #    NEW POSITION LIMITS     #
				// ##############################
				if(jsoneq(listenerMessage.pMessage, &tokList[itemNo], STR_SETTING_OBJ_MAX)) {
					debug("Position MAX");
					resetCoef=1;
					for(subItemNo=2; subItemNo<(((tokList[itemNo+1].size)<<1)+2); subItemNo=subItemNo+2){
						freeTok = &tokList[itemNo+subItemNo];
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MAX_X)){
							debug("MAX_X");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MAX X : %s",tmpBuffer);
								pointMax.pointX = (float)strtod(tmpBuffer, NULL);
								debug("Position MAX X : %f",pointMax.pointX);
							}
						}else
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MAX_Y)){
							debug("MAX_Y");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MAX Y : %s",tmpBuffer);
								pointMax.pointY = (float)strtod(tmpBuffer, NULL);
								debug("Position MAX Y : %f",pointMax.pointY);
							}
						}else
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MAX_Z)){
							debug("MAX_Y");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MAX Z : %s",tmpBuffer);
								pointMax.pointZ = (float)strtod(tmpBuffer, NULL);
								debug("Position MAX Z : %f",pointMax.pointZ);
							}
						}
					}
				}else
				if(jsoneq(listenerMessage.pMessage, &tokList[itemNo], STR_SETTING_OBJ_MIN)) {
					debug("Position MIN");
					resetCoef=1;
					for(subItemNo=2; subItemNo<(((tokList[itemNo+1].size)<<1)+2); subItemNo=subItemNo+2){
						freeTok = &tokList[itemNo+subItemNo];
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MIN_X)){
							debug("MIN_X");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MIN X : %s",tmpBuffer);
								pointMin.pointX = (float)strtod(tmpBuffer, NULL);
								debug("Position MIN X : %f",pointMin.pointX);
							}
						}else
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MIN_Y)){
							debug("MIN_Y");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MIN Y : %s",tmpBuffer);
								pointMin.pointY = (float)strtod(tmpBuffer, NULL);
								debug("Position MIN Y : %f",pointMin.pointY);
							}
						}else
						if(jsoneq(listenerMessage.pMessage, freeTok, STR_SETTING_MIN_Z)){
							debug("MIN_Z");
							if(sprintf(tmpBuffer, "%.*s", freeTok[1].end-freeTok[1].start, (char*)listenerMessage.pMessage + freeTok[1].start)){
								debug("Position MIN Z : %s",tmpBuffer);
								pointMin.pointZ = (float)strtod(tmpBuffer, NULL);
								debug("Position MIN Z : %f",pointMin.pointZ);
							}
						}
					}
				}else debug("Error in Json! JSON Object not recognised.\n");
        	//}

        	if(resetCoef){
        		computeCoef(&pointCoef, &pointMax, &pointMin, &enable);
        		resetCoef=0;
        	}

			// ##############################
			// #        NEW POSITION        #
			// ##############################
        	if(update){
            	newParameter.parameter = PID_CONSIGNE;
            	newParameter.consigne = computeConsigne(&current, &pointCoef, &enable);
            	if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
            	{
            		// Failed to post the message.
            	}// else printf("New order sent!");
            	update=0;
        	}

			// ##############################
			// #        NEW DUTY CYCLE      #
			// ##############################
        	if(getNewOrder){
            	newParameter.parameter = PID_CONSIGNE;
            	newParameter.consigne = newOrder;
            	if( xQueueSend( RegulatorQueueHDL, (void*) &newParameter, (TickType_t) 0 ) != pdPASS )
            	{
            		// Failed to post the message.
            	}// else printf("New order sent!");
            	getNewOrder=0;
        	}
        }
    }
}

