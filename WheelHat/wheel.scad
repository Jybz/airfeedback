//Air Feedback wheel

// Global resolution
$fs = 0.1;  // Don't generate smaller facets than 0.1 mm
$fa = 4;    // Don't generate larger angles than 5 degrees
//$fs = 0.4;  // Don't generate smaller facets than 0.1 mm
//$fa = 20;    // Don't generate larger angles than 5 degrees

//use <gearModule.scad>
//use <Getriebe.scad>
//use <gear.scad>
//use <MCAD/involute_gears.scad>
//use <parametric_involute_gear_v5.0.scad>

showHat = true;
showOriginWheel = false;

//######################################
//# Wheel parameters                   #
//######################################
ori_Wheel_R = 20; //mm
ori_Wheel_H = 5; //mm
ori_Wheel_zo = -ori_Wheel_H/2-0.1; //mm
ori_WheelMainScrewHole_R = 1.5; //mm
ori_WheelScrewHoles_R = 1.2; //mm
ori_WheelHole1_xo = 10.0; //mm
ori_WheelHole2_xo = 15.0; //mm
ori_WheelHole_shift_xo = 1.0; //mm
ori_WheelHole1b_xo = 11.0; //mm
ori_WheelHole2b_xo = 16.0; //mm

ori_WheelGear_Ri = 3.5; //mm inside the teeth
ori_WheelGear_Ro = 4.0; //mm ouside the teeth
ori_WheelGear_H = 5; //mm the height of the gear
ori_WheelGearHouse_R = 6.3; //mm
ori_WheelGearHouse_H = 5; //mm
ori_WheelGearHouse_zo = -3; //mm

ori_WheelGearTooth_nb = 15; //tooth

ori_WheelGearTooth_H = ori_WheelGear_Ri;
ori_WheelGearTooth_W = ori_WheelGear_Ro/2;

ori_WheelScrew_a0 = 0*45;
ori_WheelScrew_a1 = 1*45;
ori_WheelScrew_a2 = 2*45;
ori_WheelScrew_a3 = 3*45;
ori_WheelScrew_a4 = 4*45;
ori_WheelScrew_a5 = 5*45;
ori_WheelScrew_a6 = 6*45;
ori_WheelScrew_a7 = 7*45;

//######################################
//# Top Hat parameters                 #
//######################################
hat_serreCable_radius = 5.30/2;//mm
hat_serreCable_height = 8.40;//mm
hat_serreCableHole_z = hat_serreCable_height/2;
hat_outer_radius = ori_Wheel_R;
hat_thickness = hat_serreCable_height; //mm
hat_cable_radius = 2.2/2;//mm
hat_moment_distance = 18.3; //mm
hat_z0 = hat_thickness/2;
//hat_z0 = 0;
hat_serreCable_x0 = 0;//mm
hat_serreCable_y0 = hat_moment_distance-ori_WheelHole1_xo;//mm


if (showOriginWheel) Wheel();
if (showHat) Hat();

module Wheel() {
    translate([0, 0, ori_Wheel_zo]) difference() {
        union(){
            difference() {
                cylinder(r=ori_Wheel_R, h=ori_Wheel_H, center=true);
                rotate([0, 0, ori_WheelScrew_a0]) translate([                     0, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a1]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a2]) translate([                     0, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a3]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a4]) translate([                     0, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a5]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a6]) translate([                     0, 0, 0]) WheelScrewHoles();
                rotate([0, 0, ori_WheelScrew_a7]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles();
                }
            translate([0, 0, ori_WheelGearHouse_zo]) cylinder(r=ori_WheelGearHouse_R, h=ori_WheelGearHouse_H, center=true);
        }
        translate([0, 0, ori_WheelGearHouse_zo]) cylinder(r=ori_WheelMainScrewHole_R, h=ori_Wheel_H*4, center=true);
    }
    
}

module WheelScrewHoles(inner=true, outer=true) {
    union() {
        if(inner) translate([ori_WheelHole1_xo, 0, 0]) cylinder(r=ori_WheelScrewHoles_R, h=ori_Wheel_H*2, center=true);
        if(outer) translate([ori_WheelHole2_xo, 0, 0]) cylinder(r=ori_WheelScrewHoles_R, h=ori_Wheel_H*2, center=true);
    }
}


module Hat() {
    translate([0, 0, hat_z0]) difference() {
        //The "hat"
        cylinder(r=ori_Wheel_R, h=hat_thickness, center=true);
        
        //The cable chute
        rotate_extrude(){
            translate([hat_moment_distance, 0, 0]) union() {
                circle(hat_cable_radius);
                translate([hat_outer_radius/2, 0, 0])
                    square([hat_outer_radius, hat_cable_radius*2], true);
            }
        }
        
        //The screw to match the wheel
        rotate([0, 0, ori_WheelScrew_a0]) translate([                     0, 0, 0]) WheelScrewHoles(outer=false);
        rotate([0, 0, ori_WheelScrew_a1]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles(outer=true, inner=false);
        rotate([0, 0, ori_WheelScrew_a2]) translate([                     0, 0, 0]) WheelScrewHoles(outer=true, inner=false);
        rotate([0, 0, ori_WheelScrew_a3]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles(outer=true, inner=false);
        rotate([0, 0, ori_WheelScrew_a4]) translate([                     0, 0, 0]) WheelScrewHoles(outer=true);
        rotate([0, 0, ori_WheelScrew_a5]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles(outer=false);
        rotate([0, 0, ori_WheelScrew_a6]) translate([                     0, 0, 0]) WheelScrewHoles(outer=false);
        rotate([0, 0, ori_WheelScrew_a7]) translate([ori_WheelHole_shift_xo, 0, 0]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a0]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a1]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a2]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a3]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a4]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a5]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a6]) WheelScrewHoles(outer=false);
        //rotate([0, 0, ori_WheelScrew_a7]) WheelScrewHoles(outer=false);
        
        //The place for the cable clamp
        CableClamp();

        CablePath();
    }
    

}


//CableClamp();
module CableClamp() {
        translate([hat_serreCable_x0, hat_serreCable_y0, 0]){
            cylinder(r=hat_serreCable_radius, h=hat_serreCable_height, center=true);
            //sphere(hat_serreCable_radius*1.1);
            translate([-hat_serreCable_radius/2, 0, 0]) rotate([0, -90, 0])
                cylinder(hat_serreCable_radius*2, r=hat_cable_radius*1.5, r2=hat_cable_radius*0.8, true);
            translate([hat_serreCable_radius/2, 0, 0]) rotate([0, 90, 0])
                cylinder(hat_serreCable_radius*2, r=hat_cable_radius*1.5, r2=hat_cable_radius*0.8, true);
        }
}

//CablePath();
module CablePath() {
    translate([ori_WheelHole1_xo, 0, 0])//hat_serreCableHole_z])
    //difference(){
    //rotate_extrude(angle=90){
    rotate_extrude2(angle=90){//, convexity=10){
        translate([hat_moment_distance-ori_WheelHole1_xo, 0, 0]) 
        circle(hat_cable_radius*1);
    }
    translate([hat_moment_distance-(ori_WheelHole1_xo/2)-hat_cable_radius*2, hat_moment_distance-ori_WheelHole1_xo, 0]) rotate([0, -90, 0])
        cylinder(r=hat_cable_radius,h=hat_outer_radius*2);
    //    translate([-hat_outer_radius/2, 0, -hat_cable_radius])
    //        cube([hat_outer_radius,hat_outer_radius,hat_cable_radius*2], false);
    //    translate([-hat_outer_radius, -hat_outer_radius/2, -hat_cable_radius])
     //       cube([hat_outer_radius,hat_outer_radius,hat_cable_radius*2], false);
    //}
        //translate([-hat_outer_radius, -hat_outer_radius/2, -hat_cable_radius]) cube([hat_outer_radius,hat_outer_radius,hat_cable_radius*2], false);
}


//API from thehans : http://forum.openscad.org/rotate-extrude-angle-always-360-tp19035p19040.html
module rotate_extrude2(angle=360, convexity=2, size=1000) {

  module angle_cut(angle=90,size=1000) {
    x = size*cos(angle/2);
    y = size*sin(angle/2);
    translate([0,0,-size]) 
      linear_extrude(2*size) polygon([[0,0],[x,y],[x,size],[-size,size],[-size,-size],[x,-size],[x,-y]]);
  }

  // support for angle parameter in rotate_extrude was added after release 2015.03 
  // Thingiverse customizer is still on 2015.03
  angleSupport = (version_num() > 20150399) ? true : false; // Next openscad releases after 2015.03.xx will have support angle parameter
  // Using angle parameter when possible provides huge speed boost, avoids a difference operation

  if (angleSupport) {
    rotate_extrude(angle=angle,convexity=convexity)
      children();
  } else {
    rotate([0,0,angle/2]) difference() {
      rotate_extrude(convexity=convexity) children();
      angle_cut(angle, size);
    }
  }
}