#!/bin/sh

#For local debuging, start a server :
#$ ncat -l 10001 --keep-open --udp --exec "/bin/true" --output "/dev/stdout"

# char=$(xxd -l 1 -p -u /dev/urandom)
# msg=$(echo "$(echo "ibase=16;${char}" | bc)/2.55" | bc)
# echo $msg
# exit

DC=0
send=0
err=" "

IP=""
PORT=""
IP="192.168.1.161"
#IP="localhost"
IP="192.168.2.15"
PORT="10001"
LOG=~/process.log

message=""

MAX=100
LIMIT=${MAX}
LIMIT=50
MIN=2
OFF=0
#calcule random*limite/max+MIN

if [ -z ${IP} ]; then
    echo "IP address :"
    read -ip IP
fi
if [ -z ${PORT} ]; then
    echo "Port :"
    read -ip PORT
fi

while  [ "${C}" != "q" ]; do
    clear
    echo -e $err
    err=" "
    echo -e "\nProcess :\n\t[O] to open/release\n\t[L] to Write the imatricul Number\n\t[A] for maximum and tell\n\t[Z] for minimum and tell\n\t[R] to select a random number\n\t[P] to send the selected random number.\n"
    echo "Messages destination : ${IP}:${PORT}"
    echo "[Q] to quit."
    echo "[A] DC=100"
    echo "[Z] DC=0"
    echo "[R] Get random number."
    echo "[P] Send the value."
    echo "[O] Off."
    echo "[L] Log a matricul number."
    echo -e "R:${VALpourcent}%"
    
    read -s -n 1 C
    case $C in
    'a')
        DC=$((${LIMIT}+${MIN}))
        send=1
        ;;
    'z')
        DC=${MIN}
        send=1
        ;;
    'r')
        char=$(xxd -l 1 -p -u /dev/urandom)
        VALpourcent=$(echo "$(echo "ibase=16;${char}" | bc)/2.55" | bc)
        VALcalibrated=$(echo "${VALpourcent}*${LIMIT}/${MAX}+${MIN}" | bc)
        err="Random : ${VALpourcent}\tCalibrated : ${VALcalibrated}"
        DC=${VALcalibrated}
        send=0
        ;;
    'p')
        DC=${VALcalibrated}
        send=1
        ;;
    'o')
        DC=${OFF}
        echo -e "Open..." >> ${LOG}
        send=1
        ;;
    'l')
        read matricul
        echo ${matricul} >> ${LOG}
        ;;
    *)
        err="Character not allowed."
        ;;
    esac

    if [ 1 -eq ${send} ]; then
        send=0
        message=$(echo -e "{ \"DC\": ${DC} } ")
        echo -e "${VALpourcent}\t${message}" >> ${LOG}
        echo ${message} | ncat -C -u ${IP} ${PORT}
        err=$(echo ${message})
    fi

done
